package praktikum11;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;

public class Yleminek extends Applet {
	
	public void paint(Graphics g) {
		gradient(g, new Color(255,0,0), new Color(1,0,0));
	}
	
	public void gradient(Graphics g, Color c1, Color c2) {
		for (int i = 0; i <= this.getHeight(); i++) {
			//int red = (i*(c1.getRed()/c2.getRed()))/this.getHeight();  // jagab 255 värvi akna kõrguse peale ära
			int color = (i*255/this.getHeight());
			g.setColor(new Color(color, color, color));
			g.drawLine(0, i, this.getWidth(), i);
		}
	}

}
