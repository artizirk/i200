package praktikum2;

public class NimePikkus {

	public static void main(String[] args) {
		System.out.println("Sisesta mingi nimi ja ma ütlen sulle selle pikkuse");
		System.out.printf("Sisesta nimi: ");
		String nimi = lib.TextIO.getlnString();
		int nimePikkus = nimi.length();
		System.out.println("Sisestatud nime pikkus on " + nimePikkus);
	}

}
