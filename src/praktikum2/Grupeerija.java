package praktikum2;

public class Grupeerija {

	public static void main(String[] args) {
		System.out.printf("Sisesta inimeste arv: ");
		int inimeste_arv = lib.TextIO.getlnInt();
		
		System.out.printf("Sisesta grupi suurus: ");
		int grupi_suurus = lib.TextIO.getlnInt();
		
		int gruppide_arv = inimeste_arv / grupi_suurus;
		int inimeste_jääk = inimeste_arv % grupi_suurus;
		
		System.out.printf("Kui %d inimest jagada gruppidesse kus igas grupi on %d inimest\n",
				inimeste_arv, grupi_suurus);
		System.out.printf("Siis oleks kokku %d gruppi ja üle jääks %d inimest",
				gruppide_arv, inimeste_jääk);
	}

}
