package praktikum2;

public class Korrutaja {

	public static void main(String[] args) {
		System.out.printf("Sisesta esimene arv: ");
		int arv1 = lib.TextIO.getlnInt();
		
		System.out.printf("Sisesta teine arv: ");
		int arv2 = lib.TextIO.getlnInt();
		
		System.out.println("Arvude " + arv1 + " ja " + arv2 + " korrutis on " + arv1*arv2);
	}
}

//                                        
//   █████    ▒██▒    ██▒  ▒██    ▒██▒   
//   █████    ▓██▓    ██▓  ▓██    ▓██▓   
//      ██    ████    ▒██  ██▒    ████   
//      ██    ████    ▒██  ██▒    ████   
//      ██   ▒█▓▓█▒    ██ ░██    ▒█▓▓█▒  
//      ██   ▓█▒▒█▓    ██▒▒██    ▓█▒▒█▓  
//      ██   ██  ██    ██▒▒██    ██  ██  
//      ██   ██████    ▒████▒    ██████  
//      ██  ░██████░   ░████░   ░██████░ 
//█▒   ▒██  ▒██  ██▒    ████    ▒██  ██▒ 
//███████▓  ███  ███    ████    ███  ███ 
//░█████▒   ██▒  ▒██    ▓██▓    ██▒  ▒██
//
                                    
                                   