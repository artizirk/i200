package praktikum7;

public class Kuulujutud {

	public static void main(String[] args) {
		String[] naistenimed = {"Mari", "Katrin", "Liisu", "Margit"};
		String[] meestenimed = {"Peeter", "Toomas", "Tanel", "Margus"};
		String[] tegusõnad  = {"jalutasid", "ujusid", "laulsid", "tantsisid"};
		
		String naisenimi = naistenimed[praktikum5.Metodid.randomRange(0, naistenimed.length-1)];
		String mehenimi  = meestenimed[praktikum5.Metodid.randomRange(0, meestenimed.length-1)];
		String tegusõna  = tegusõnad[praktikum5.Metodid.randomRange(0, tegusõnad.length-1)];
		
		System.out.printf("%s ja %s %s.", naisenimi, mehenimi, tegusõna);
	}

}
