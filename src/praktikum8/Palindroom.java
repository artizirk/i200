package praktikum8;

public class Palindroom {

	public static void main(String[] args) {
		System.out.print("Kirjuta sõna:");
		
		String sona = lib.TextIO.getlnString();
		String backwarsSona = new StringBuilder(sona).reverse().toString();
		
		if (sona.equals(backwarsSona)) {
			System.out.println("Tegu on palindroomiga");
		} else {
			System.out.println("See ei ole palindroom");
		}
	}

}
