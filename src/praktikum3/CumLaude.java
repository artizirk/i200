package praktikum3;

public class CumLaude {

	public static void main(String[] args) {
		
		// Küsime kasutaja käest hindeid ujukomaarvudena 
		System.out.printf("Sisesta keskmine hinne: ");
		double keskminehinne = lib.TextIO.getlnDouble();
		System.out.printf("Sisesta lõputöö hinne: ");
		double  lõputööhinne = lib.TextIO.getlnDouble();
		
		// Kasutajapoolt sisestatud numbrite reeglitele vastavuse kontroll
		if (keskminehinne > 5.0) {
			System.out.printf("Keskmine hinne ei tohi olla üle 5, teie seisestasite %.1f %n", keskminehinne);
			return; // välju main funktsioonist ja lõpeta programmi töö
		} else if (lõputööhinne >5.0) {
			System.out.printf("Lõputöö hinne ei tohi olla üle 5, teie seisestasite %.1f %n", lõputööhinne);
			return;
		} else if (keskminehinne < 0.0) {
			System.out.printf("Keskmine hinne ei tohi olla üle negatiivne, teie seisestasite %.1f %n", keskminehinne);
			return;
		} else if (lõputööhinne < 0.0) {
			System.out.printf("Lõputöö hinne ei tohi olla üle negatiivne, teie seisestasite %.1f %n", lõputööhinne);
			return;
		}
		
		// Ülessanne ise
		if ((keskminehinne >= 4.5) && (lõputööhinne == 5.0)) {
			System.out.println("Palju õnne, saad Cum-Laude oma diplomile");
		} else {
			System.out.println("Sina kahjuks oma hinnetega Cum-Laude-t enda diplomile");
		}
		

	}

}
