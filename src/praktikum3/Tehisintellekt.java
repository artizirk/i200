package praktikum3;

public class Tehisintellekt {

	public static void main(String[] args) {
		// Küsime kasutaja käest mõned vanused
		System.out.printf("Sisesta üks vanus: ");
		int esimenevanus = lib.TextIO.getlnInt();
		System.out.printf("Sisesta teine vanus: ");
		int teinevanus = lib.TextIO.getlnInt();
		
		// Sisendi valideerimine
		if ((esimenevanus < 0) || (teinevanus < 0)) {
			System.out.println("Vanus ei tohi olla negatiivne");
			return;
		} 
		
		// Teeme järgnevad vanusevahe testid lihtsamaks eemaldades endest mitme erineva vanuse vahe testimise
		int esimenevanusevahe = esimenevanus-teinevanus;
		int teinevanusevahe = teinevanus-esimenevanus;
		int vanusevahe = 0; // vanusevahe mida me hakkame testima
		
		if (esimenevanus < teinevanus) {
			vanusevahe = teinevanusevahe;
		} else {
			vanusevahe = esimenevanusevahe;
		}
		
		
		if ((vanusevahe > 5) && (vanusevahe < 10)) {
			System.out.println("Krõbe");
		} else if (vanusevahe >= 10) {
			System.out.println("Väga krõbe");
		} else {
			System.out.println("Sobib");
		}
		
	}

}
