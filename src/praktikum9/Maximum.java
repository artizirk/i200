package praktikum9;

public class Maximum {

	public static void main(String[] args) {
		int[] massiiv = {1, 3, 6, 7, 8, 3, 5, 7, 21, 3};
		int[][] neo = {
			    {1, 3, 6, 7},
			    {2, 3, 3, 1},
			    {17, 4, 5, 0},
			    {-20, 13, 16, 17}
			};
		System.out.printf("1D masiivi max on %d%n", praktikum7.Maksimum.maksimum(massiiv));
		System.out.printf("2D masiivi max on %d%n", praktikum7.Maksimum.maksimum(neo));
	}

}
